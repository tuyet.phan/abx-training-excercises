
//https://leetcode.com/problems/reverse-integer/
//written by: minhtuyet224

class Solution {
public:
    int reverse(int x)
    {   
        int sum=0;
        int a;
        while(x!=0)
        {      
            a= x%10;          
            x=x/10;
            if (sum > INT_MAX/10 || (sum == INT_MAX / 10 && a > 7)) return 0;
            if (sum < INT_MIN/10 || (sum == INT_MIN / 10 && a < -8)) return 0;
            sum=sum*10+a;
        }
       
	return sum;
    }
};
