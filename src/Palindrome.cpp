
//https://leetcode.com/problems/palindrome-number/
//written by: minhtuyet224

class Solution {
public:
    bool isPalindrome(int x)  {
        int sum=0;
        int tem=x;
        if(x<0) 
		return false;
        else 
        {
            while(x!=0)
            {
            	sum=sum*10+x%10;
            	x=x/10;
            }

           if (sum==tem)
		return true;
           else    
		return false;
        }            
    }
};
